# Failed slurm jobs + Snakemake

This setup allows snakemake to detect failed jobs on the cluster. 
Snakemake will track the status of the job and then respond accordingly. 

Previously when cluster jobs failed (time limit exceeded, memory exceeded, failed 
otherwise), snakemake was waiting for the output file indefinitely. 


## Test run

# test if it is working

You can use this as a positive control. 
```
sh submit_slurm.sh all
```

This will create A.txt


# time outs
In this test run, the job will time out after a minute or two

```
sh submit_slurm.sh timeout
```

This will create B.txt

# clean up

```
snakemake clean
```

This will clean up A.txt and B.txt


## How does it work?

Snakemake will use the status.py script to check the cluster job instead of 
just waiting for the output file. 

https://snakemake.readthedocs.io/en/stable/tutorial/additional_features.html#using-cluster-status




