#!/bin/bash
#SBATCH --hint=nomultithread

cluster_config_file=cluster.yaml
status_script=./status.py
singularity_top_dir=/g
latency_wait=30
restart_times=1
local_cores=1
mail_type=FAIL
max_jobs=1000
job_name='status'
snakemake_logs=${job_name}_sm_$(date +'%Y_%m_%d')
cluster_logs=${job_name}_logs_$(date +'%Y_%m_%d')
cluster_errs=${job_name}_errs_$(date +'%Y_%m_%d')

mkdir -p $snakemake_logs $cluster_logs $cluster_errs
CMD='snakemake  --nolock --reason --latency-wait '"$latency_wait"' --notemp --verbose --printshellcmds  --rerun-incomplete --restart-times '"$restar$
echo $CMD
eval $CMD
