rule all:
        output:
                "A.txt"
        shell:
                "sleep 10 && date > {output}"

rule timeout:
        output:
                "B.txt"
        shell:
                "sleep 560 && date > {output}"

rule clean:
        shell:
                "rm -f A.txt B.txt"
